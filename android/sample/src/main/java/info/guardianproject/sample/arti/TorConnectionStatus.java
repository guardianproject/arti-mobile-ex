// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: MIT

package info.guardianproject.sample.arti;

public enum TorConnectionStatus {
    TOR,
    DIRECT,
    ERROR,
}
